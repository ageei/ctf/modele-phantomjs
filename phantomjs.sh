#!/bin/sh
##
exec /opt/phantomjs/bin/phantomjs \
	--local-to-remote-url-access=true \
	--web-security=false \
	--disk-cache=false \
	/opt/phantomjs/visit-page.js \
	"${@}"
