FROM php:7.0-apache

RUN	apt-get -qq update && \
	apt-get -y install libfontconfig && \
	apt-get clean && rm -rf /var/lib/apt/lists


COPY	phantomjs /opt/phantomjs
COPY	phantomjs.sh /usr/bin/phantomjs
RUN	chmod +x /usr/bin/phantomjs
