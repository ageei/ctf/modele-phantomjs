# Docker image for XSS challenges using phantomjs

This image provides a Docker image on which to build a
XSS challenge. It builds on php:7.0-apache and installs
PhantomJs and a page loading script. The browser can be
invoked directly with the URL to load.

## Usage

Build your own Docker image with this example Dockerfil:

```
FROM registry.gitlab.com/ageei/ctf/modele-phantomjs

COPY htdocs /var/www/html
```

An example is provided in `tests`.
